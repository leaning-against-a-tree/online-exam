/*
Navicat MySQL Data Transfer

Source Server         : xyh
Source Server Version : 50527
Source Host           : localhost:3306
Source Database       : onlineeduexam

Target Server Type    : MYSQL
Target Server Version : 50527
File Encoding         : 65001

Date: 2023-05-21 15:20:01
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_classes
-- ----------------------------
DROP TABLE IF EXISTS `tbl_classes`;
CREATE TABLE `tbl_classes` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `classes_name` varchar(255) DEFAULT '' COMMENT '专业名称',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user` int(11) DEFAULT NULL COMMENT '创建人',
  `deleted` bit(1) DEFAULT b'0' COMMENT '是否删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for tbl_exam
-- ----------------------------
DROP TABLE IF EXISTS `tbl_exam`;
CREATE TABLE `tbl_exam` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '试卷主键编号',
  `exam_name` varchar(255) DEFAULT NULL COMMENT '试卷名称',
  `exam_total` double(50,2) DEFAULT NULL COMMENT '试卷总分',
  `begin_time` datetime DEFAULT NULL COMMENT '开考时间',
  `end_time` datetime DEFAULT NULL COMMENT '结束时间',
  `exam_time` int(10) DEFAULT NULL COMMENT '考试时长，单位：min',
  `subject_id` int(11) DEFAULT NULL COMMENT '所属科目编号',
  `create_user` int(11) DEFAULT NULL COMMENT '创建人编号',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `deleted` bit(1) DEFAULT b'0' COMMENT '是否删除标志：0代表未删除，1代表删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for tbl_exam_classes_rel
-- ----------------------------
DROP TABLE IF EXISTS `tbl_exam_classes_rel`;
CREATE TABLE `tbl_exam_classes_rel` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `classes_id` int(11) DEFAULT NULL,
  `exam_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for tbl_exam_question_answer
-- ----------------------------
DROP TABLE IF EXISTS `tbl_exam_question_answer`;
CREATE TABLE `tbl_exam_question_answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键编号',
  `answer` text COMMENT '作答答案',
  `score` double(50,2) DEFAULT NULL COMMENT '得分',
  `correct_error` bit(1) DEFAULT NULL COMMENT '是否正确：0代表错误 1代表正确',
  `exam_id` int(11) DEFAULT NULL COMMENT '试卷编号',
  `question_id` int(11) DEFAULT NULL COMMENT '题目编号',
  `create_user` int(11) DEFAULT NULL COMMENT '作答者',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=141 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for tbl_exam_record
-- ----------------------------
DROP TABLE IF EXISTS `tbl_exam_record`;
CREATE TABLE `tbl_exam_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `exam_id` int(11) DEFAULT NULL COMMENT '试卷id',
  `total_score` double(255,0) DEFAULT NULL COMMENT '总分',
  `do_time` int(11) DEFAULT NULL COMMENT '用时，单位：秒',
  `create_user` int(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `iscorrected` bit(1) DEFAULT b'0' COMMENT '是否被批改：0：未批改 1：已批改',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for tbl_log
-- ----------------------------
DROP TABLE IF EXISTS `tbl_log`;
CREATE TABLE `tbl_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '日志主键编号',
  `log_content` varchar(255) DEFAULT NULL COMMENT '日志内容',
  `username` varchar(255) DEFAULT NULL COMMENT '日志操作人',
  `is_login` bit(1) DEFAULT b'0' COMMENT '是否是登录',
  `create_user` int(11) DEFAULT NULL COMMENT '操作人编号',
  `create_time` datetime DEFAULT NULL COMMENT '日志创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=240 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for tbl_message
-- ----------------------------
DROP TABLE IF EXISTS `tbl_message`;
CREATE TABLE `tbl_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键编号',
  `title` varchar(255) DEFAULT NULL COMMENT '主题',
  `message_content` text COMMENT '公告内容',
  `receive_num` int(11) DEFAULT NULL COMMENT '应收人数',
  `actual_receive_num` int(11) DEFAULT NULL COMMENT '实际接收人数/已读人数',
  `create_user` int(11) DEFAULT NULL COMMENT '公告发布人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for tbl_message_user
-- ----------------------------
DROP TABLE IF EXISTS `tbl_message_user`;
CREATE TABLE `tbl_message_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键编号',
  `receive_id` int(11) DEFAULT NULL COMMENT '接收人编号',
  `create_user` int(11) DEFAULT NULL COMMENT '发布人编号',
  `readed` bit(1) DEFAULT NULL COMMENT '是否已读标志 0 代表未读 1 代表已读',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `read_time` datetime DEFAULT NULL COMMENT '阅读时间',
  `message_id` int(11) DEFAULT NULL COMMENT '消息编号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for tbl_question
-- ----------------------------
DROP TABLE IF EXISTS `tbl_question`;
CREATE TABLE `tbl_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键编号',
  `question_name` text NOT NULL COMMENT '题目描述',
  `question_type` int(10) DEFAULT NULL COMMENT '题目类别：1单选题、2多选题、3判断题、4简答题',
  `question_score` double(255,2) DEFAULT NULL COMMENT '题目分数',
  `answer_a` text COMMENT '答案A',
  `answer_b` text,
  `answer_c` text,
  `answer_d` text,
  `difficulty` double(5,2) DEFAULT '1.00' COMMENT '难度',
  `correct_answer` text NOT NULL COMMENT '正确答案',
  `question_desc` text COMMENT '题目解析',
  `subject_id` int(11) DEFAULT NULL COMMENT '所属科目编号',
  `create_user` int(11) DEFAULT NULL COMMENT '创建人编号',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `deleted` bit(1) DEFAULT b'0' COMMENT '是否删除0代表未删除，1代表删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for tbl_question_exam_rel
-- ----------------------------
DROP TABLE IF EXISTS `tbl_question_exam_rel`;
CREATE TABLE `tbl_question_exam_rel` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `exam_id` int(11) DEFAULT NULL COMMENT '试卷编号',
  `question_id` int(11) DEFAULT NULL COMMENT '题目编号',
  `question_score` double(10,2) DEFAULT '0.00' COMMENT '题目分数',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=244 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for tbl_question_practice
-- ----------------------------
DROP TABLE IF EXISTS `tbl_question_practice`;
CREATE TABLE `tbl_question_practice` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键编号',
  `practice_answer` varchar(255) DEFAULT NULL COMMENT '练习作答答案',
  `subject_id` int(11) DEFAULT NULL COMMENT '学科的Id(作为保留字段存在）',
  `question_id` int(11) DEFAULT NULL COMMENT '题目编号',
  `create_user` int(11) DEFAULT NULL COMMENT '练习作答者',
  `correct` bit(1) DEFAULT NULL COMMENT '是否正确，0代表错误 1代表正确',
  `create_time` datetime DEFAULT NULL COMMENT '作答时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for tbl_subject
-- ----------------------------
DROP TABLE IF EXISTS `tbl_subject`;
CREATE TABLE `tbl_subject` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '科目编号',
  `subject_name` varchar(255) NOT NULL COMMENT '科目名称',
  `item_order` varchar(255) DEFAULT '1' COMMENT '排序优先级',
  `deleted` bit(1) DEFAULT b'0' COMMENT '删除标志：0代表未删除，1代表删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for tbl_user
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `account` varchar(30) NOT NULL COMMENT '账号',
  `password` varchar(255) NOT NULL COMMENT '登录密码',
  `username` varchar(255) NOT NULL COMMENT '用户名',
  `sex` int(11) DEFAULT '0' COMMENT '0代表男，1代表女',
  `user_email` varchar(255) DEFAULT NULL COMMENT '邮箱',
  `role` int(10) NOT NULL DEFAULT '2' COMMENT '权限编号：0代表管理员，1代表老师，2代表学生用户',
  `user_status` bit(1) NOT NULL DEFAULT b'0' COMMENT '用户状态：0代表正常 1代表封禁',
  `birth_day` date DEFAULT NULL COMMENT '生日',
  `image_path` varchar(255) DEFAULT NULL COMMENT '头像路径',
  `deleted` bit(1) DEFAULT b'0' COMMENT '是否被删除，0代表未删除1代表已删除',
  `classes_id` int(11) DEFAULT '0' COMMENT '班级ID',
  `create_user` varchar(255) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for tbl_wrong
-- ----------------------------
DROP TABLE IF EXISTS `tbl_wrong`;
CREATE TABLE `tbl_wrong` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `question_id` int(11) DEFAULT NULL COMMENT '题目ID',
  `answer` text COMMENT '错误答案',
  `create_user` int(11) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL COMMENT '科目Id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;
