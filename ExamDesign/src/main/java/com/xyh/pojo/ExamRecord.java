package com.xyh.pojo;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * 
 * @TableName tbl_exam_record
 */
@Data
public class ExamRecord implements Serializable {
    /**
     * id
     */
    private Integer id;

    /**
     * 试卷id
     */
    private Integer examId;

    /**
     * 总分
     */
    private Double totalScore;

    /**
     * 用时，单位：秒
     */
    private Integer doTime;

    /**
     * 
     */
    private Integer createUser;

    /**
     * 
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" , timezone = "GMT+8")
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
     * 是否被批改：0：未批改 1：已批改
     */
    private Boolean iscorrected;

    private static final long serialVersionUID = 1L;
}