package com.xyh.pojo;

import lombok.Data;

import java.io.Serializable;

/**
 * 
 * @TableName tbl_subject
 */
@Data
public class Subject implements Serializable {
    /**
     * 科目编号
     */
    private Integer id;

    /**
     * 科目名称
     */
    private String subjectName;

    /**
     * 排序优先级
     */
    private String itemOrder;

    /**
     * 删除标志：0代表未删除，1代表删除
     */
    private Boolean deleted;

   /* *//**
     * 年级号
     *//*
    private Integer levelGrade;

    *//**
     * 年级名称
     *//*
    private String levelName;*/

    private static final long serialVersionUID = 1L;

    /**
     * 科目编号
     */
    public Integer getId() {
        return id;
    }

    /**
     * 科目编号
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 科目名称
     */
    public String getSubjectName() {
        return subjectName;
    }

    /**
     * 科目名称
     */
    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    /**
     * 排序优先级
     */
    public String getItemOrder() {
        return itemOrder;
    }

    /**
     * 排序优先级
     */
    public void setItemOrder(String itemOrder) {
        this.itemOrder = itemOrder;
    }

    /**
     * 删除标志：0代表未删除，1代表删除
     */
    public Boolean getDeleted() {
        return deleted;
    }

    /**
     * 删除标志：0代表未删除，1代表删除
     */
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

}