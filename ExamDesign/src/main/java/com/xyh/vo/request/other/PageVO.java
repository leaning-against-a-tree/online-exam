package com.xyh.vo.request.other;

import lombok.Data;

@Data
public class PageVO {
    private Long pageIndex;
    private Long pageSize;
}
