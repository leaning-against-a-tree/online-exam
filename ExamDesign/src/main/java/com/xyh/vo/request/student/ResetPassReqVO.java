package com.xyh.vo.request.student;

import lombok.Data;

/**
 * 密码重置的VO
 */
@Data
public class ResetPassReqVO {

    private String account;
    private String userEmail;
    private String vailCode;

}
