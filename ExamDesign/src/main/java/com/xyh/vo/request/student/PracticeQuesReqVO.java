package com.xyh.vo.request.student;

import lombok.Data;

@Data
public class PracticeQuesReqVO {

    private Integer subjectId;
    private Integer lastQuestion;
    private Integer questionId;
    private Integer userId;
}
