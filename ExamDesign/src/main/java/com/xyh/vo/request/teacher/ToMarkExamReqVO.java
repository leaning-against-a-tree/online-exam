package com.xyh.vo.request.teacher;

import com.xyh.vo.request.other.PageVO;
import lombok.Data;

/**
 * 教师查询待批改的试卷VO请求
 */
@Data
public class ToMarkExamReqVO extends PageVO {

    private String examName;
    private String classesId;

}
