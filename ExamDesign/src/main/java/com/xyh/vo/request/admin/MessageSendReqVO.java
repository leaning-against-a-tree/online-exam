package com.xyh.vo.request.admin;

import lombok.Data;

import java.util.List;

/**
 * 公告发送的类
 */
@Data
public class MessageSendReqVO {
    private String title;
    private String  messageContent;

    // 接收者的编号
    private List<Integer> receiveUserIds;

}
