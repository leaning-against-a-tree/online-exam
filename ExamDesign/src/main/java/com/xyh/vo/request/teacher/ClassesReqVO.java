package com.xyh.vo.request.teacher;

import com.xyh.vo.request.other.PageVO;
import lombok.Data;

@Data
public class ClassesReqVO extends PageVO {

    private String classesName;
}
