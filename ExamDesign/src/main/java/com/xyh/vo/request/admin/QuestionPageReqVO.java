package com.xyh.vo.request.admin;

import com.xyh.vo.request.other.PageVO;
import lombok.Data;

import java.util.List;

/**
 * 教师对于题目查询的分页请求类
 */
@Data
public class QuestionPageReqVO extends PageVO {
    private String questionName;
    private Integer questionType;
    private List<Integer> subjectIds;
    private Boolean deleted;
}
