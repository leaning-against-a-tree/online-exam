package com.xyh.vo.request.teacher;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * 自动组卷的请求VO
 */
@Data
public class AutoGroupReqVO {

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" , timezone = "GMT+8")
    private Date beginTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" , timezone = "GMT+8")
    private Date endTime;

    private Integer generateType;

    private Double examTotal;

    private Integer subjectId;

    private String examName;

    private Integer judgeNum;

    private Double judgeScore;

    private Integer mutilNum;

    private Double mutilScore;

    private Integer singleNum;

    private Double singleScore;

    private Integer writtenNum;

    private Double writtenScore;

    private List<Integer> classesIds;
}
