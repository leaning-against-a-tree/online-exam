package com.xyh.vo.request.admin;

import com.xyh.vo.request.other.PageVO;
import lombok.Data;

/**
 * 日志相关的
 */
@Data
public class LogReqVO extends PageVO {
    private Integer createUser;
    private String username;
}
