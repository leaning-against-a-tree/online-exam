package com.xyh.vo.request.admin;

import com.xyh.vo.request.other.PageVO;
import lombok.Data;

/**
 * 用户查询请求的VO
 */

@Data
public class UserReqVO extends PageVO {
    private String username;
    private Integer role;
    private Integer status;
}
