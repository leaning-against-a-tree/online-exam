package com.xyh.vo.request.student;

import com.xyh.vo.request.other.PageVO;
import lombok.Data;

/**
 * 通用查询的VO
 */

@Data
public class QueryReqVO extends PageVO {

    private Integer subjectId;
    private Integer userId;
    private String examName;

}
