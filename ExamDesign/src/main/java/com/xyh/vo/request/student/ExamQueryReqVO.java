package com.xyh.vo.request.student;

import com.xyh.vo.request.other.PageVO;
import lombok.Data;

/**
 * 学生界面考试的请求参数
 */
@Data
public class ExamQueryReqVO extends PageVO {

    // 用户的班级
    private Integer classesId;
    // 试卷是否已经完结
    private Integer isfinished;
    //条件的学科的ID
    private Integer subjectId;
}
