package com.xyh.vo.request.admin;

import com.xyh.vo.request.other.PageVO;
import lombok.Data;

/**
 * 试卷的分页的
 */
@Data
public class ExamPageReqVO extends PageVO {
    private Integer userLevel;
    private Boolean deleted;
    private Integer subjectId;
    private String examName;
}
