package com.xyh.vo.request.admin;

import com.xyh.vo.request.other.PageVO;
import lombok.Data;

/**
 * 消息列表分页查询的VO
 */
@Data
public class MessageReqVO extends PageVO {
    private String sendUserName;
}
