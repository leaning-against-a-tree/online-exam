package com.xyh.vo.request.admin;

import com.xyh.vo.request.other.PageVO;
import lombok.Data;

/**
 * 对于学科的请求VO
 */
@Data
public class SubjectReqVO extends PageVO {
    private String subjectName;
}
