package com.xyh.vo.response.teacher;

import com.xyh.vo.response.other.KeyValue;
import lombok.Data;

import java.util.List;

@Data
public class IndexVO {

    private List<KeyValue> quesData;

    private List<KeyValue> examData;
}
