package com.xyh.vo.response.student;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * 学生端首页的信息的返回
 */
@Data
public class StuExamRespVO {

    private Integer id;
    private String examName;
    private String subjectName;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date beginTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date endTime;
    private Integer questionSum;
    private Double examTotal;
    private Integer examTime;
    private Integer isfinished;

    /**
     * 是否对某厂考试完成了
     * true 代表已经考过
     * false 代表还未进行考试
     */
    private Boolean completed;
}
