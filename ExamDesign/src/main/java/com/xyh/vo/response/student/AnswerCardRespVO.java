package com.xyh.vo.response.student;

import lombok.Data;

@Data
public class AnswerCardRespVO {

    private Integer questionId;
    private Boolean correct;
    private Integer itemOrder;
}
