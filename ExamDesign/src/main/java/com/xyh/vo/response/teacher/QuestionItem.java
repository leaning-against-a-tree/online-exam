package com.xyh.vo.response.teacher;

import lombok.Data;

/**
 * 一个题目项
 */

@Data
public class QuestionItem {
    private Integer id;
    private String questionName;
    private String answerA;
    private String answerB;
    private String answerC;
    private String answerD;
    private Integer questionType;
    private Double questionScore;
}
