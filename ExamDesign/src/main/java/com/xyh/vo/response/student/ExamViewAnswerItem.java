package com.xyh.vo.response.student;

import lombok.Data;

/**
 * 试卷查看的VO
 */
@Data
public class ExamViewAnswerItem {

    private Integer id;
    private Integer questionId;
    private Double score;
    private Boolean correctError;
    private String questionName;
    private Integer questionType;
    private Double questionScore;
    private String answerA;
    private String answerB;
    private String answerC;
    private String answerD;
    private String correctAnswer;
    private String answer;
    private String questionDesc;
}
