package com.xyh.vo.response.student;

import com.xyh.pojo.ExamRecord;
import lombok.Data;

/**
 * 用于返回考试记录中的数据
 */
@Data
public class RecordRespVO extends ExamRecord {
    private String examName;
    private String subjectName;
}
