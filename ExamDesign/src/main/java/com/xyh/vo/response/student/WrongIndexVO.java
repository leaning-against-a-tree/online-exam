package com.xyh.vo.response.student;

import lombok.Data;

/**
 * 错题本首页的VO
 */
@Data
public class WrongIndexVO {
    private Integer id;
    private String subjectName;
    private Integer wrongCount;
}
