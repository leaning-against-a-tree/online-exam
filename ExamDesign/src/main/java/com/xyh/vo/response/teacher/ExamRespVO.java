package com.xyh.vo.response.teacher;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * 试卷预览响应的VO
 */
@Data
public class ExamRespVO {

    private Integer id;

    private String examName;

    private Double examTotal;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date beginTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date endTime;

    private Integer subjectId;

    private String subjectName;

    private Integer examTime;

    private List<QuestionItem> questionItems;

    /**
     * 所属班级
     */
    private List<Integer> classesIds;

}
