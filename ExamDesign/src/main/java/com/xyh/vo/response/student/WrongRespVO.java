package com.xyh.vo.response.student;

import lombok.Data;

import java.util.List;

/**
 * 错题本阅读页进行返回的VO
 */
@Data
public class WrongRespVO {
    private String subjectName;
    private List<WrongDataItem> wrongs;
}
