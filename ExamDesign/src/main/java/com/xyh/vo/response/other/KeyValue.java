package com.xyh.vo.response.other;

import lombok.Data;

/**
 * 管理员对于题目的数据
 */
@Data
public class KeyValue {
    private Integer value;
    private String name;
}
