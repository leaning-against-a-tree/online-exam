package com.xyh.vo.response.student;

import com.xyh.pojo.Question;
import lombok.Data;

/**
 * 单个错题项
 */
@Data
public class WrongDataItem {
    private Integer id;
    private String answer;
    private Integer questionType;
    private String questionName;
    private String answerA;
    private String answerB;
    private String answerC;
    private String answerD;
    private String correctAnswer;
    private Double questionScore;
    private Integer difficulty;
    private String questionDesc;
}
