package com.xyh.vo.response.admin;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class QuestionPageRespVO {
    private Integer id;
    private String questionName;
    private Integer questionType;
    private Double questionScore;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;
    private String subjectName;
    private String username;
}
