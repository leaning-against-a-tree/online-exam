package com.xyh.vo.response.student;

import lombok.Data;

import java.util.List;

/**
 * 试卷查看的VO
 */
@Data
public class ExamViewVO {

    private Integer examId;
    private String examName;
    private Integer doTime;
    private Double totalScore;
    private Double examTotal;
    private Boolean iscorrected;
    private List<ExamViewAnswerItem> answers;
}
