package com.xyh.vo.response.student;

import lombok.Data;

@Data
public class PracticeQuestionRespVO {

    // 题目的id
    private Integer id;
    private String questionName;
    private Integer questionType;
    private String answerA;
    private String answerB;
    private String answerC;
    private String answerD;
    private String correctAnswer;
    private String questionDesc;

    private String practiceAnswer;
    private Boolean correct;
    private Integer subjectId;

}
