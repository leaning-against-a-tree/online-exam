package com.xyh.vo.response.teacher;

import com.xyh.pojo.Classes;
import lombok.Data;

@Data
public class ClassesRespVO extends Classes {

    private String username;
}
