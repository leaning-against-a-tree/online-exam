package com.xyh.vo.response.admin;

import com.xyh.pojo.Log;
import lombok.Data;

@Data
public class LogRespVO extends Log {
    private String account;
    private String username;
}
