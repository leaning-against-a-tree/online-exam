package com.xyh.vo.response.admin;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * 公告列表的响应类
 */

@Data
public class MessageRespVO {
    private Integer id;
    private String title;
    private String messageContent;
    private String sendUserName;
    private Integer receiveNum;
    private String actualReceiveNum;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;
}
