package com.xyh.vo.response.teacher;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * 对于教师需要进行试卷批改的回复VO
 */
@Data
public class ToMarExamRecordRespVO {

    private Integer id;

    private String examName;

    private String username;

    private String classesName;


    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;

}
