package com.xyh.vo.response.student;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * 练习册首页的数据返回VO
 */
@Data
public class PracticeListRespVO {

    private Integer id;

    private String subjectName;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;

    private Integer questionCount;

    private Integer finishedCount;

}
