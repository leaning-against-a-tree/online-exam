package com.xyh.vo.response.student;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * 信息的返回的VO
 */

@Data
public class StuMessageRespVO {
    private Integer id;
    private String title;
    private Boolean readed;
    private String username;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;
    private  String messageContent;
}
