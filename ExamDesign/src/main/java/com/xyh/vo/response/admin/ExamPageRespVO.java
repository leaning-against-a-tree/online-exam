package com.xyh.vo.response.admin;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * 对于教师端的试卷分页的响应数据
 */
@Data
public class ExamPageRespVO {

    private Integer id;

    private String examName;

    private String subjectName;

//    private String levelName;

    private String classesName;

    private Double examTotal;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date beginTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date endTime;

    private String username;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;

}
