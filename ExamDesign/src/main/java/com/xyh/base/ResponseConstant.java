package com.xyh.base;

/**
 * 响应的编码格式
 */
public class ResponseConstant {

    public static final int SUCCESS = 200;

    public static final int FAILED = 0;

}
