package com.xyh.base;

/**
 * 角色权限的常量
 */
public class RoleConstant {

    public static final String USER = "USER";

    public static final String TEACHER = "TEACHER";

    public static final String ADMIN = "ADMIN";

}
