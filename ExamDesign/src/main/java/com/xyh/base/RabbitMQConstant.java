package com.xyh.base;

/**
 * MQ的常量
 */
public interface RabbitMQConstant {

    String LOG_QUEUE = "log.queue";

    String EXAM_EXCHANGE = "exam_exchange";

    String MESSAGE_USER_QUEUE = "exam_message_user";

    String MESSAGE_USER_KEY = "message_user";

    String LOG_KEY = "log";

}
