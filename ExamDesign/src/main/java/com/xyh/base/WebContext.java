package com.xyh.base;

import com.xyh.config.security.vo.LoginUser;
import com.xyh.pojo.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;


/**
 * 用来获取当前用户的类
 */
@Slf4j
@Component
public class WebContext {


    public User getCurrentUser(){
        LoginUser principal = (LoginUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        return principal.getUser();
    }

}
