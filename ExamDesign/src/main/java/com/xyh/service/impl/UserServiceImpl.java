package com.xyh.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xyh.base.RabbitMQConstant;
import com.xyh.base.RedisConstant;
import com.xyh.base.WebContext;
import com.xyh.config.cache.RedisCache;
import com.xyh.config.security.vo.LoginUser;
import com.xyh.config.sysLog.UserEvent;
import com.xyh.pojo.Log;
import com.xyh.pojo.User;
import com.xyh.service.EmailService;
import com.xyh.service.UserService;
import com.xyh.mapper.UserMapper;
import com.xyh.utils.JwtUtil;
import com.xyh.utils.Utils;
import com.xyh.vo.request.other.UpdatePassReqVO;
import com.xyh.vo.request.admin.UserReqVO;
import com.xyh.vo.request.student.ResetPassReqVO;
import io.jsonwebtoken.Claims;
import org.apache.logging.log4j.util.Strings;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
* @author xyh
* @description 针对表【tbl_user】的数据库操作Service实现
* @createDate 2022-12-27 22:18:13
*/
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User>
implements UserService{

    private AuthenticationManager authenticationManager;
    private RedisCache redisCache;
    private PasswordEncoder passwordEncoder;
    private EmailService emailService;
    private ApplicationEventPublisher publisher;
    private RabbitTemplate rabbitTemplate;
    private WebContext context;

    @Value("${img.basePath}")
    private String basePath;

    @Autowired
    public UserServiceImpl(AuthenticationManager authenticationManager, RedisCache redisCache,PasswordEncoder passwordEncoder,
                           ApplicationEventPublisher publisher,EmailService emailService,RabbitTemplate rabbitTemplate,WebContext context) {
        this.authenticationManager = authenticationManager;
        this.redisCache = redisCache;
        this.passwordEncoder = passwordEncoder;
        this.publisher = publisher;
        this.emailService = emailService;
        this.rabbitTemplate = rabbitTemplate;
        this.context = context;
    }

    @Override
    public IPage<User> findUserByCondition(UserReqVO vo) {
        LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(!Strings.isEmpty(vo.getUsername()),User::getUsername,vo.getUsername())
                .eq(User::getRole,vo.getRole())
                .eq(vo.getStatus() != null,User::getUserStatus,vo.getStatus())
                .eq(User::getDeleted,0)
                .orderByDesc(User::getCreateTime);
        IPage<User> page = new Page<>(vo.getPageIndex(),vo.getPageSize());
        IPage<User> pages = baseMapper.selectPage(page, wrapper);
        if(pages.getPages() < vo.getPageIndex()){
            vo.setPageIndex(1L);
            page = new Page<>(vo.getPageIndex(),vo.getPageSize());
            pages = baseMapper.selectPage(page,wrapper);
        }

        return pages;
    }

    @Override
    public boolean editUser(User user) {
        int count = 0;
        String secretPass = passwordEncoder.encode(user.getPassword());
        user.setPassword(secretPass);
        if(user.getId() == null){
            user.setDeleted(false);
            user.setImagePath("default.jpg");
            sendLogMsg(new Log(getUser().getUsername()+"新增用户"+user.getUsername(),getUser().getUsername(),getUser().getId(),false,new Date()));
            count = baseMapper.insert(user);
        }else{
            //清除缓存
            redisCache.deleteObject("user:"+user.getId());
            sendLogMsg(new Log(getUser().getUsername()+"对"+user.getUsername()+"信息进行了编辑",getUser().getUsername(),getUser().getId(),false,new Date()));
            count = baseMapper.updateById(user);
        }
        return count > 0;
    }

    @Override
    public void updateStatus(Integer[] ids) {
        List<User> list = baseMapper.selectBatchIds(Arrays.asList(ids));
        list = list.stream().map(item ->{
            item.setUserStatus(item.getUserStatus() == 0 ? 1 : 0);
            return item;
        }).collect(Collectors.toList());
        list.stream().forEach(item -> baseMapper.updateById(item));
    }

    @Override
    public Map<String,String> login(User user) {
        //进行用户认证
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(user.getAccount(),user.getPassword());
        Authentication authenticate = authenticationManager.authenticate(authenticationToken);

        LoginUser loginUser = (LoginUser) authenticate.getPrincipal();
        User one = loginUser.getUser();
        //验证成功，那么就将user保存到redis中,生成token
        String account = one.getAccount();
        String token = JwtUtil.createJWT(account);
        Map<String,String> map = new HashMap<>();
        map.put("token",token);
        // 存储登录用户
        String redisKey = "edu-login:"+account;
        redisCache.setCacheObject(redisKey,loginUser,180, TimeUnit.MINUTES);

        //发布日志
        Log log = new Log("登录了Edu考试系统",one.getUsername(),one.getId(),true,new Date());
        publisher.publishEvent(new UserEvent(log));
        return map;
    }


    @Override
    @Transactional
    public boolean deleteUser(Integer id) {
        //清理缓存
        redisCache.deleteObject("user:"+id);
        // 查找用户，异步发送日志
        User user = baseMapper.selectById(id);
        sendLogMsg(new Log(getUser().getUsername()+"删除了用户"+user.getUsername(),getUser().getUsername(),getUser().getId(),false,new Date()));
        //
        return baseMapper.removeById(id) > 0;
    }


    @Override
    public List<User> getUserByUserName(String username) {
        LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
        wrapper
                .eq(User::getRole,2)
                .eq(User::getDeleted,false)
                .like(Strings.isNotEmpty(username),User::getUsername,username);
        return baseMapper.selectList(wrapper);
    }

    @Override
    @Transactional
    public void updateAvator(Integer userId, String path) {
        User user = baseMapper.selectById(userId);
        String imagePath = user.getImagePath() == null ? "" : user.getImagePath();
        imagePath = basePath+imagePath;
        File file = new File(imagePath);
        if(file.exists()){
            file.delete();
        }
        user.setImagePath(path);
        baseMapper.updateById(user);
    }

    /**
     * 更新个人信息
     * @param user
     */
    @Override
    public void updateUser(User user) {
        String account = user.getAccount();
        baseMapper.updateUser(user);
        String redisKey = RedisConstant.PREFIX+account;

        Object obj = redisCache.getCacheObject(redisKey);
        LoginUser loginUser = JSON.parseObject(JSON.toJSONString(obj),LoginUser.class);
        loginUser.setUser(user);
        redisCache.setCacheObject(redisKey,loginUser,180,TimeUnit.MINUTES);
    }

    /**
     *
     * @param vo
     * @param userId
     * @return
     */
    @Override
    @Transactional
    public boolean updatePass(UpdatePassReqVO vo,Integer userId) {
        User user = baseMapper.selectById(userId);
        if(!passwordEncoder.matches(vo.getOldPass(),user.getPassword())){
            return false;
        }
        String pass = passwordEncoder.encode(vo.getNewPass());
        user.setPassword(pass);

        //进行redis的数据更新
        String redisKey = RedisConstant.PREFIX+user.getAccount();
        Object obj = redisCache.getCacheObject(redisKey);
        LoginUser login = JSON.parseObject(JSON.toJSONString(obj),LoginUser.class);
        login.getUser().setPassword(pass);
        redisCache.setCacheObject(redisKey,login,180,TimeUnit.MINUTES);
        return baseMapper.updateById(user) > 0;
    }

    @Override
    public void logout(String token) {
        Claims claims = null;
        try {
            claims = JwtUtil.parseJWT(token);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String account = claims.getSubject();
        String redisKey = "edu-login:"+account;
        boolean flag = redisCache.deleteObject(redisKey);
    }

    @Override
    public Boolean resetPass(ResetPassReqVO vo) {
        LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(User::getAccount,vo.getAccount())
                .eq(User::getUserEmail,vo.getUserEmail())
                .eq(User::getRole,2);
        User user = baseMapper.selectOne(wrapper);
        if(Objects.isNull(user)){
            return false;
        }
        // 验证码过期
        String code = redisCache.getCacheObject(user.getUserEmail());
        if(Strings.isEmpty(code)){
            return false;
        }
        // 验证成功
        if(code.equals(vo.getVailCode())){
            String pass = Utils.resetPass();
            String secretPass = passwordEncoder.encode(pass);
            user.setPassword(secretPass);
            baseMapper.updateById(user);
            // 异步发送信息，避免响应速度慢
            new Thread(()->{
                emailService.sendRestPass(vo.getUserEmail(), pass);
            }).start();
            return true;
        }
        return false;
    }

    @Override
    public User getUserById(Integer id) {
        User user = null;
        Object obj = redisCache.getCacheObject("user:" + id);
        if(!Objects.isNull(obj)){
            user = (User) obj;
        }else{
            user = baseMapper.selectById(id);
            redisCache.setCacheObject("user:"+id,user,10,TimeUnit.MINUTES);
        }
        return user;
    }

    //TODO 后续统一处理
    private void sendLogMsg(Log log){
        rabbitTemplate.convertAndSend(RabbitMQConstant.EXAM_EXCHANGE,RabbitMQConstant.LOG_KEY,log);
    }

    private User getUser(){
        return context.getCurrentUser();
    }

}
