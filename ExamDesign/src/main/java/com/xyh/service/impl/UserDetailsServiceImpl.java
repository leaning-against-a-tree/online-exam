package com.xyh.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.xyh.base.RoleEnum;
import com.xyh.config.security.vo.LoginUser;
import com.xyh.mapper.UserMapper;
import com.xyh.pojo.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * 第二个核心对象之UserDetailService
 */
@Slf4j
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private UserMapper userMapper;

    @Autowired
    public UserDetailsServiceImpl(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    /**
     *
     * @param account
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String account) throws UsernameNotFoundException {
        LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(User::getAccount,account).eq(User::getDeleted,false);
        User user = userMapper.selectOne(wrapper);


        if(Objects.isNull(user)){
            throw new UsernameNotFoundException("用户未注册");
        }


        if(user.getUserStatus() == 1){
            throw new LockedException("账号被封禁");
        }

        List<String> authorities = new ArrayList<>();
        authorities.add(RoleEnum.fromCode(user.getRole()).getRoleName());

        return new LoginUser(user,authorities);
    }
}
