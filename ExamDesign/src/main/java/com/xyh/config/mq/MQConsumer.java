package com.xyh.config.mq;

import com.xyh.base.RabbitMQConstant;
import com.xyh.pojo.Log;
import com.xyh.pojo.MessageUser;
import com.xyh.service.LogService;
import com.xyh.service.MessageService;
import com.xyh.service.MessageUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 消息队列消息的消费者
 */
@Slf4j
@Component
public class MQConsumer {


    private LogService logService;

    private MessageUserService messageUserService;

    @Autowired
    public MQConsumer(LogService logService,MessageUserService messageUserService) {
        this.logService = logService;
        this.messageUserService = messageUserService;
    }

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(name = RabbitMQConstant.LOG_QUEUE),
            exchange = @Exchange(name = RabbitMQConstant.EXAM_EXCHANGE),
            key = RabbitMQConstant.LOG_KEY
    ))
    @Transactional(rollbackFor = RuntimeException.class)
    public void consumer(Log userLog){
        logService.save(userLog);
    }

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(name = RabbitMQConstant.MESSAGE_USER_QUEUE),
            exchange = @Exchange(name = RabbitMQConstant.EXAM_EXCHANGE),
            key = RabbitMQConstant.MESSAGE_USER_KEY
    ))
    @Transactional(rollbackFor = RuntimeException.class)
    public void consumer(List<MessageUser> list){
        list.forEach(item ->{
            messageUserService.save(item);
        });
//        logService.save(userLog);
    }

}
