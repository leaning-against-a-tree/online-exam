package com.xyh.config.mvc;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

@Configuration
public class MVCConfig extends WebMvcConfigurationSupport {


    @Override
    protected void addViewControllers(ViewControllerRegistry registry) {
        registry.addRedirectViewController("/","/student/index.html");
        registry.addRedirectViewController("/student","/student/index.html");
        registry.addRedirectViewController("/teacher","/teacher/index.html");
        registry.addRedirectViewController("/admin","/admin/index.html");
    }

    /**
     * 配置资源映射
     * @param registry
     */
    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/avatar/**")
                .addResourceLocations("file:D:\\IDEA\\ExamDesign\\src\\main\\resources\\static\\avatar\\")
                .setCachePeriod(31556926);
    }
}
