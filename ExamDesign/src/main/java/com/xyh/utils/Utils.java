package com.xyh.utils;


import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

public class Utils {

    private  static Random random = new Random();

    public static String  getUUID(){
        return UUID.randomUUID().toString().replace("-","");
    }

    /**
     * 验证码的生成
     * @return
     */
    public static String generatorValidationCode(){
        Integer code = new Random().nextInt(999999);
        if(code< 100000){
            return String.valueOf(code + 100000);
        }
        return String.valueOf(code);
    }


    public static String resetPass(){
        List<String> list = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            list.add(String.valueOf(random.nextInt(10)));
        }
        list.add(getUUID().substring(0,6));
        return list.stream().collect(Collectors.joining(""));
    }

    /**
     * 得到图片的路径
     * @param fileName
     * @param request
     * @return
     */
    public static String filePath(String fileName, HttpServletRequest request){
        String url = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+"/avatar/";

        if(fileName.contains(url)){
            return fileName;
        }
        if(Objects.isNull(fileName)){
            return "";
        }
        return url+fileName;
    }
}
