package com.xyh.utils;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * 用于文件名的获取
 */
@Data
@Slf4j
@Component
public class FilePathUtil {

    @Value("${server.port}")
    private  Integer port;

    @Value("${img.url}")
    private  String url;

    @Value("${img.mapping-name}")
    private  String mappingName;

    /**
     * 获取带有服务器与端口的名的文件名
     * @param fileName
     * @return string
     */
    public String getFileName(String fileName){
        String baseUrl = "http://"+url+":"+port+"/"+mappingName;
        if(fileName.contains(baseUrl)){
            return fileName;
        }
        if(Objects.isNull(fileName)){
            return "";
        }
        return baseUrl+fileName;
    }

    /**
     *
     * @return
     */
    public static String getSSS(){
        return "";
    }


}
