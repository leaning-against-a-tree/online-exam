package com.xyh.controller.admin;

import com.xyh.base.ResultAPI;
import com.xyh.service.IndexService;
import com.xyh.vo.response.admin.AdminIndexRespVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@PreAuthorize("hasAnyRole('ADMIN')")
@RequestMapping("/admin/dashboard/")
@RestController("adminDashBoardController")
public class DashBoardController {

    private IndexService indexService;

    @Autowired
    public DashBoardController(IndexService indexService) {
        this.indexService = indexService;
    }

    @GetMapping("index")
    public ResultAPI<AdminIndexRespVO> getIndex(){
        AdminIndexRespVO adminIndex = indexService.getAdminIndex();
        return new ResultAPI<>(200,"查询成功",adminIndex);
    }

}
