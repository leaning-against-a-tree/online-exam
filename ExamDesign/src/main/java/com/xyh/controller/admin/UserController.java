package com.xyh.controller.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xyh.base.ResultAPI;
import com.xyh.pojo.User;
import com.xyh.service.UserService;
import com.xyh.utils.FilePathUtil;
import com.xyh.vo.request.admin.UserReqVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Slf4j
@PreAuthorize("hasAnyRole('ADMIN')")
@RequestMapping("/admin/user/")
@RestController("AdminUserController")
public class UserController {

    private UserService userService;
    private FilePathUtil util;

    @Autowired
    public UserController(UserService userService,FilePathUtil util) {
        this.userService = userService;
        this.util = util;
    }

    @PostMapping("page/list")
    public ResultAPI<IPage<User>> getPage(@RequestBody(required = false) UserReqVO vo, HttpServletRequest request){
        IPage<User> page = userService.findUserByCondition(vo);
        page.getRecords().forEach(item->{
            item.setImagePath(util.getFileName(item.getImagePath()));
        });
        return new ResultAPI<>(200,"查询成功",page);
    }

    @GetMapping("select/{id}")
    public ResultAPI<User> getByIds(@PathVariable Integer id,HttpServletRequest request){
        User user = userService.getById(id);
//        String url = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+"/avatar/"+user.getImagePath();
        user.setImagePath(util.getFileName(user.getImagePath()));
        return new ResultAPI<>(200,"查询成功", user);
    }

    @PostMapping("edit")
    public ResultAPI<String> editUser(@RequestBody User user){
        boolean flag = userService.editUser(user);
        if(flag){
            return new ResultAPI<>(200,"操作成功","");
        }else{
            return new ResultAPI<>(0,"操作失败","");
        }
    }

    @PostMapping("updateStatus")
    public ResultAPI<String> updateStatus(@RequestBody Integer[] ids){
            userService.updateStatus(ids);
        return new ResultAPI<>(200,"更新状态成功","");
    }

    @GetMapping("delete/{id}")
    public ResultAPI<String> deleteById(@PathVariable Integer id){
        boolean flag = userService.deleteUser(id);
        if(flag){
            return new ResultAPI<>(200,"删除成功","");
        }
        return new ResultAPI<>(0,"删除失败","");
    }

    @GetMapping("selectUser/{username}")
    public ResultAPI<List<User>> getUserByUserName(@PathVariable String username){
        List<User> list = userService.getUserByUserName(username);

        return new ResultAPI<>(200,"查询成功",list);
    }

}
