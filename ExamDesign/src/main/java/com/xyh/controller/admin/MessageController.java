package com.xyh.controller.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xyh.base.ResultAPI;
import com.xyh.base.WebContext;
import com.xyh.service.MessageService;
import com.xyh.vo.request.admin.MessageReqVO;
import com.xyh.vo.request.admin.MessageSendReqVO;
import com.xyh.vo.response.admin.MessageRespVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@Slf4j
@PreAuthorize("hasAnyRole('ADMIN')")
@RequestMapping("/admin/message/")
@RestController("adminMessageController")
public class MessageController {

    private MessageService messageService;

    @Autowired
    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }



    @PostMapping("page/list")
    public ResultAPI<IPage<MessageRespVO>> pageList(@RequestBody  MessageReqVO vo){
        return new ResultAPI<>(200,"查询成功", messageService.getPageList(vo));
    }

    @PostMapping("send")
    public ResultAPI<String> sendMessage(@RequestBody MessageSendReqVO vo){
        messageService.sendMessage(vo);
        return new ResultAPI<>(200,"发送成功","");
    }
}
