package com.xyh.controller.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xyh.base.ResultAPI;
import com.xyh.base.WebContext;
import com.xyh.config.cache.RedisCache;
import com.xyh.pojo.Subject;
import com.xyh.service.SubjectService;
import com.xyh.vo.request.admin.SubjectReqVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@Slf4j
@PreAuthorize("hasAnyRole('ADMIN')")
@RequestMapping("/admin/subject/")
@RestController("adminSubjectController")
public class SubjectController {

    private SubjectService subjectService;

    @Autowired
    public SubjectController(SubjectService subjectService ) {
        this.subjectService = subjectService;
    }

    @PostMapping("/page/list")
    public ResultAPI<IPage<Subject>> getPageList(@RequestBody SubjectReqVO vo){
        IPage<Subject> pageList = subjectService.getPageList(vo);
        return new ResultAPI<>(200,"查询成功",pageList);
    }

    @PostMapping("delByIds")
    public ResultAPI<String> delByIds(@RequestBody  Integer[] ids){
        if(ids == null || ids.length <= 0){
            return new ResultAPI<>(0,"删除失败","");
        }
        subjectService.delByIds(ids);
        return new ResultAPI<>(200,"删除成功","");
    }

    @GetMapping("select/{id}")
    public ResultAPI<Subject> getSubjectById(@PathVariable Integer id){
        Subject subject = subjectService.getSubjectById(id);
        return new ResultAPI<>(0,"查询失败",subject);
    }

    @PostMapping("edit")
    public ResultAPI<String> edit(@RequestBody Subject subject){
        boolean flag = subjectService.editSubject(subject);
        if(flag){
            return new ResultAPI<>(200,"操作成功","");
        }
        return new ResultAPI<>(0,"操作失败","");
    }
}
