package com.xyh.controller.student;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xyh.base.ResultAPI;
import com.xyh.base.WebContext;
import com.xyh.service.MessageService;
import com.xyh.service.MessageUserService;
import com.xyh.vo.request.other.PageVO;
import com.xyh.vo.response.student.StuMessageRespVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@Slf4j
@PreAuthorize("hasRole('USER')")
@RequestMapping("/student/message/")
@RestController("studentMessageController")
public class MessageController {

    private MessageService messageService;
    private MessageUserService messageUserService;

    @Autowired
    public MessageController(MessageService messageService, MessageUserService messageUserService) {
        this.messageService = messageService;
        this.messageUserService = messageUserService;
    }

    @GetMapping("getUnread")
    public ResultAPI<Integer> getUnreadCount(){
        Integer count = messageUserService.selectUnreadCount();
        return  new ResultAPI<>(200,"查询成功",count);
    }

    @PostMapping("list")
    public ResultAPI<IPage<StuMessageRespVO>> getMsgList(@RequestBody PageVO vo){
        IPage<StuMessageRespVO> page = messageService.selectStuList(vo);
        return new ResultAPI<>(200,"查询成功",page);
    }

    /**
     *标记消息已读
     * @param id
     * @return
     */
    @GetMapping("read/{id}")
    public ResultAPI<String> readMessage(@PathVariable Integer id){
        messageService.readMessage(id);
        return new ResultAPI<>(200,"","");
    }


}
