package com.xyh.controller.student;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xyh.base.ResponseConstant;
import com.xyh.base.ResultAPI;
import com.xyh.base.WebContext;
import com.xyh.pojo.Wrong;
import com.xyh.service.WrongService;
import com.xyh.vo.request.student.QueryReqVO;
import com.xyh.vo.response.student.WrongIndexVO;
import com.xyh.vo.response.student.WrongRespVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@Slf4j
@PreAuthorize("hasRole('USER')")
@RequestMapping("/student/wrong/")
@RestController("studentWrongController")
public class WrongController {

    private WrongService wrongService;

    @Autowired
    public WrongController(WrongService wrongService) {
        this.wrongService = wrongService;
    }

    @PostMapping("list")
    public ResultAPI<IPage<WrongIndexVO>> list(@RequestBody QueryReqVO vo){

        IPage<WrongIndexVO> page = wrongService.pageList(vo);
        return new ResultAPI<>(ResponseConstant.SUCCESS,"",page);
    }

    @PostMapping("wrongList")
    public ResultAPI<WrongRespVO> wrongList(@RequestBody QueryReqVO vo){
        WrongRespVO wrongRespVO = wrongService.selectWrongsByPage(vo);
        return new ResultAPI<>(ResponseConstant.SUCCESS,"",wrongRespVO);
    }

    @GetMapping("/remove/{id}")
    public ResultAPI<Boolean>removeWrong(@PathVariable Integer id){
        Boolean flag = wrongService.delWrong(id);
        return new ResultAPI<>(ResponseConstant.SUCCESS,"移除成功",flag);
    }



}
