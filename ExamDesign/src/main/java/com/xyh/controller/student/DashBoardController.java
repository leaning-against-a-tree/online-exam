package com.xyh.controller.student;

import com.xyh.base.ResponseConstant;
import com.xyh.base.ResultAPI;
import com.xyh.base.WebContext;
import com.xyh.pojo.User;
import com.xyh.service.ExamRecordService;
import com.xyh.service.ExamService;
import com.xyh.vo.response.student.ChartVO;
import com.xyh.vo.response.student.StuExamRespVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@PreAuthorize("hasRole('USER')")
@RequestMapping("/student/dashboard/")
@RestController("studentDashBoardController")
public class DashBoardController {

    private ExamService examService;

    private ExamRecordService examRecordService;
//    private WebContext webContext;

    @Autowired
    public DashBoardController(ExamService examService,ExamRecordService examRecordService) {
        this.examService = examService;
        this.examRecordService = examRecordService;
    }


    /**
     * 得到学生界面首页的试卷
     * @return
     */
    @GetMapping("index")
    public ResultAPI<List<StuExamRespVO>>index(){
        List<StuExamRespVO> vos = examService.selectRecentExam();
        return new ResultAPI<>(200,"查询成功",vos);
    }

    @GetMapping("chart")
    public ResultAPI<ChartVO> chart(){
        ChartVO chartVO = examRecordService.selectIndexChart();
        return new ResultAPI<>(ResponseConstant.SUCCESS,"",chartVO);
    }

}
