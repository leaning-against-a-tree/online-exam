package com.xyh.controller.student;

import com.xyh.base.ResultAPI;
import com.xyh.base.WebContext;
import com.xyh.service.SubjectService;
import com.xyh.vo.response.other.KeyValue;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@PreAuthorize("hasRole('USER')")
@RequestMapping("/student/subject/")
@RestController("/studentSubjectController")
public class SubjectController {

    private SubjectService subjectService;

    @Autowired
    public SubjectController(SubjectService subjectService) {
        this.subjectService = subjectService;
    }

    @GetMapping("/list")
    public ResultAPI<List<KeyValue>> getSubjects(){
        List<KeyValue> list = subjectService.selectSubjects();
        return new ResultAPI<>(200,"请求成功",list);
    }

}
