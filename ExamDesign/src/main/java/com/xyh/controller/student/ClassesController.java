package com.xyh.controller.student;

import com.xyh.base.ResponseConstant;
import com.xyh.base.ResultAPI;
import com.xyh.pojo.Classes;
import com.xyh.service.ClassesService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RequestMapping("/student/classes/")
@RestController("StudentClassesController")
public class ClassesController {

    private ClassesService classesService;

    @Autowired
    public ClassesController(ClassesService classesService) {
        this.classesService = classesService;
    }

    @PostMapping("/list/{classesName}")
    public ResultAPI<List<Classes>> listClass(@PathVariable String classesName){

        List<Classes> list = classesService.selectByClassName(classesName);

        return new ResultAPI<>(ResponseConstant.SUCCESS,"",list);
    }
}
