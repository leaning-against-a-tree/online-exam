package com.xyh.controller.student;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xyh.base.ResponseConstant;
import com.xyh.base.ResultAPI;
import com.xyh.base.WebContext;
import com.xyh.pojo.Question;
import com.xyh.pojo.User;
import com.xyh.service.ExamService;
import com.xyh.service.QuestionService;
import com.xyh.vo.request.student.ExamQueryReqVO;
import com.xyh.vo.request.student.ExamQuesReqVO;
import com.xyh.vo.response.student.StuExamRespVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@PreAuthorize("hasRole('USER')")
@RequestMapping("/student/exam/")
@RestController("studentExamController")
public class ExamController {

    private ExamService examService;
    private QuestionService questionService;
//    private WebContext webContext;

    @Autowired
    public ExamController(ExamService examService,QuestionService questionService) {
        this.examService = examService;
        this.questionService = questionService;
    }

    @PostMapping("/list")
    public ResultAPI<IPage<StuExamRespVO>> getStudentExam(@RequestBody ExamQueryReqVO vo){

        IPage<StuExamRespVO> page = examService.selectStudentExam(vo);
        return  new ResultAPI<>(200,"查询成功",page);
    }

    /**
     * 得到试卷上的题目合集
     * @param examId
     * @return
     */
    @GetMapping("/question/list/{examId}")
    public ResultAPI<List<Question>> getQuestionList(@PathVariable Integer examId){
        List<Question> questions = questionService.selectQuestionList(examId);
        return new ResultAPI<>(ResponseConstant.SUCCESS,"",questions);
    }


    /**
     * 自动批改试卷（客观题）
     * @param vo
     * @return 系统得分
     */
    @PostMapping("correct")
    public ResultAPI<String> correctExam(@RequestBody ExamQuesReqVO vo){
        Double score = examService.correctExam(vo);
        return new ResultAPI<>(ResponseConstant.SUCCESS,"",String.valueOf(score));
    }

}
