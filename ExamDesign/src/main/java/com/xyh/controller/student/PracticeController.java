package com.xyh.controller.student;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xyh.base.ResponseConstant;
import com.xyh.base.ResultAPI;
import com.xyh.base.WebContext;
import com.xyh.pojo.User;
import com.xyh.service.QuestionPracticeService;
import com.xyh.vo.request.student.PracticeQuesReqVO;
import com.xyh.vo.request.student.QueryReqVO;
import com.xyh.vo.response.student.AnswerCardRespVO;
import com.xyh.vo.response.student.PracticeListRespVO;
import com.xyh.vo.response.student.PracticeQuestionRespVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@PreAuthorize("hasRole('USER')")
@RequestMapping("/student/practice/")
@RestController("studentPracticeController")
public class PracticeController {

    private QuestionPracticeService practiceService;

    @Autowired
    public PracticeController(QuestionPracticeService practiceService) {
        this.practiceService = practiceService;
    }

    @PostMapping("list")
    public ResultAPI<IPage<PracticeListRespVO>> list(@RequestBody QueryReqVO vo){
        IPage<PracticeListRespVO> page = practiceService.selectPracticeList(vo);
        return new ResultAPI<>(200,"",page);
    }

    /**
     * 得到答题卡的初始数据
     * @param id
     * @return
     */
    @GetMapping("answerCard/{id}")
    public ResultAPI<List<AnswerCardRespVO>> answerCard(@PathVariable Integer id){
        List<AnswerCardRespVO> list = practiceService.selectCard(id);
        return new ResultAPI<>(200,"",list);
    }

    @PostMapping("question")
    public ResultAPI<PracticeQuestionRespVO> search(@RequestBody PracticeQuesReqVO vo){
        PracticeQuestionRespVO respVO = practiceService.selectPracticeQuestion(vo);
        return new ResultAPI<>(200,"",respVO);
    }

    /**
     * 批改题目
     * @param vo
     * @return
     */
    @PostMapping("correct")
    public ResultAPI<Boolean> correctQuestion(@RequestBody PracticeQuestionRespVO vo){
        Boolean flag = practiceService.correctQuestion(vo);
        return new ResultAPI<>(ResponseConstant.SUCCESS,"",flag);
    }

}
