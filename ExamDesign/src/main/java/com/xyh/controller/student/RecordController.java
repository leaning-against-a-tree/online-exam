package com.xyh.controller.student;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xyh.base.ResponseConstant;
import com.xyh.base.ResultAPI;
import com.xyh.base.WebContext;
import com.xyh.pojo.User;
import com.xyh.service.ExamRecordService;
import com.xyh.vo.request.student.QueryReqVO;
import com.xyh.vo.response.student.ExamViewVO;
import com.xyh.vo.response.student.RecordRespVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@Slf4j
@PreAuthorize("hasRole('USER')")
@RequestMapping("/student/record/")
@RestController("studentRecordController")
public class RecordController {

    private ExamRecordService examRecordService;

    @Autowired
    public RecordController(ExamRecordService examRecordService) {
        this.examRecordService = examRecordService;
    }

    @PostMapping("list")
    public ResultAPI<IPage<RecordRespVO>> list(@RequestBody QueryReqVO vo){
        IPage<RecordRespVO> page = examRecordService.selectPageList(vo);
        return new ResultAPI(ResponseConstant.SUCCESS,"",page);
    }

    @GetMapping("select/{examId}")
    public ResultAPI<ExamViewVO> select(@PathVariable Integer examId){
        ExamViewVO examViewVO = examRecordService.selectExamView(examId);
        return new ResultAPI<>(ResponseConstant.SUCCESS,"",examViewVO);
    }
}
