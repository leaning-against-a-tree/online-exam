package com.xyh.controller.teacher;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xyh.base.ResponseConstant;
import com.xyh.base.ResultAPI;
import com.xyh.base.WebContext;
import com.xyh.pojo.Classes;
import com.xyh.service.ClassesService;
import com.xyh.vo.request.teacher.ClassesReqVO;
import com.xyh.vo.response.teacher.ClassesRespVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Slf4j
@PreAuthorize("hasAnyRole('TEACHER')")
@RequestMapping("/teacher/classes/")
@RestController("teacherClassesController")
public class ClassesController {

    private ClassesService classesService;

//    private WebContext webContext;

    @Autowired
    public ClassesController(ClassesService classesService) {
        this.classesService = classesService;
//        this.webContext = webContext;
    }


    @PostMapping("list")
    public ResultAPI<IPage<ClassesRespVO>> list(@RequestBody ClassesReqVO vo){
        IPage<ClassesRespVO> page = classesService.selectPage(vo);
        return new ResultAPI<>(ResponseConstant.SUCCESS,"",page);
    }

    @GetMapping("select/{id}")
    public ResultAPI<Classes> selectById(@PathVariable Integer id){
        Classes classes = classesService.getById(id);
        return new ResultAPI<>(ResponseConstant.SUCCESS,"",classes);
    }

    @PostMapping("edit")
    public ResultAPI<String> edit(@RequestBody Classes vo){
        classesService.edit(vo);
        return new ResultAPI<>(ResponseConstant.SUCCESS,"操作成功","");
    }

    @PostMapping("dels")
    public ResultAPI<String> delBatch(@RequestBody List<Integer> ids){
        classesService.delBatch(ids);
        return new ResultAPI<>(ResponseConstant.SUCCESS,"删除成功","");
    }
}
