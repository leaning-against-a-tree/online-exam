package com.xyh.controller.teacher;

import com.xyh.base.ResponseConstant;
import com.xyh.base.ResultAPI;
import com.xyh.service.ExamService;
import com.xyh.service.IndexService;
import com.xyh.service.QuestionService;
import com.xyh.vo.response.teacher.IndexVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@PreAuthorize("hasAnyRole('TEACHER')")
@RequestMapping("/teacher/dashboard/")
@RestController("teacherDashBoardController")
public class DashBoardController {

   private IndexService indexService;

   @Autowired
    public DashBoardController(IndexService indexService) {
        this.indexService = indexService;
    }

    @GetMapping("index")
    public ResultAPI<IndexVO> getIndex(){
        IndexVO vo = indexService.getTeacherIndex();
        return new ResultAPI<>(ResponseConstant.SUCCESS,"",vo);
    }
}
