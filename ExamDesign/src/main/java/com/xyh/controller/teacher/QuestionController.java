package com.xyh.controller.teacher;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xyh.base.ResultAPI;
import com.xyh.base.WebContext;
import com.xyh.config.sysLog.SysLog;
import com.xyh.pojo.Question;
import com.xyh.service.QuestionService;
import com.xyh.vo.request.admin.AddQuestionPageReqVO;
import com.xyh.vo.response.admin.AddQuestionPageRespVO;
import com.xyh.vo.request.admin.QuestionPageReqVO;
import com.xyh.vo.response.admin.QuestionPageRespVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@PreAuthorize("hasAnyRole('TEACHER')")
@RequestMapping("/teacher/question/")
@RestController("teacherQuestionController")
public class QuestionController {

    private QuestionService questionService;

    @Autowired
    public QuestionController(QuestionService questionService) {
        this.questionService = questionService;
    }

    @PostMapping("/page/list")
    public ResultAPI<IPage<QuestionPageRespVO> > pageList(@RequestBody QuestionPageReqVO vo){
        IPage<QuestionPageRespVO> pages = questionService.getPageList(vo);
        return new ResultAPI<>(200,"查询成功",pages);
    }

    @SysLog(content = "对题目进行了删除")
    @PostMapping("delByIds")
    public  ResultAPI<String> delByIds(@RequestBody List<Integer> ids){
        questionService.delByIds(ids);
        return new ResultAPI<>(200,"删除成功","");
    }

    /**
     * 题目预览
     * @param id
     * @return
     */
    @GetMapping("preview/{id}")
    public ResultAPI<Question> preview(@PathVariable Integer id){
        Question question = questionService.previewQuestion(id);
        return new ResultAPI<>(200,"查询成功",question);
    }

    /**
     *
     * @param vo
     * @return
     */
    @PostMapping("edit")
    public ResultAPI<String> editOrAdd(@RequestBody Question vo){
        boolean flag = questionService.editOrAdd(vo);
        if(flag){
            return new ResultAPI<>(200,"操作成功","");
        }
        return new ResultAPI<>(0,"操作失败，请刷新重试","");
    }

    /**
     *
     * 根据id获取题目信息
     * @param id
     * @return
     */
    @GetMapping("getQuestionById/{id}")
    public ResultAPI<Question> getQuestionById(@PathVariable Integer id){
        Question question = questionService.previewQuestion(id);
        return new ResultAPI<>(200,"查询成功",question);
    }

    /**
     * 添加题目的分页
     * @param vo
     * @return
     */
    @PostMapping("add/page/list")
    public ResultAPI<IPage<AddQuestionPageRespVO>> addPageList(@RequestBody AddQuestionPageReqVO vo){
        IPage<AddQuestionPageRespVO> pageList = questionService.getAddPageList(vo);
        return new ResultAPI<>(200,"查询成功",pageList);
    }
}
