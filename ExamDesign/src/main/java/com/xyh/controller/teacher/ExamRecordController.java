package com.xyh.controller.teacher;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xyh.base.ResponseConstant;
import com.xyh.base.ResultAPI;
import com.xyh.base.WebContext;
import com.xyh.service.ExamRecordService;
import com.xyh.vo.request.teacher.ToMarkExamReqVO;
import com.xyh.vo.response.student.ExamViewAnswerItem;
import com.xyh.vo.response.student.ExamViewVO;
import com.xyh.vo.response.teacher.ToMarExamRecordRespVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@PreAuthorize("hasAnyRole('TEACHER')")
@RequestMapping("/teacher/record/")
@RestController("TeacherExamRecordController")
public class ExamRecordController {

    private ExamRecordService examRecordService;


    @Autowired
    public ExamRecordController(ExamRecordService examRecordService) {
        this.examRecordService = examRecordService;
    }


    /**
     * 查看待批改的试卷列表
     * @param vo
     * @return
     */
    @PostMapping("list")
    public ResultAPI<IPage<ToMarExamRecordRespVO>> pageList(@RequestBody ToMarkExamReqVO vo){

        IPage<ToMarExamRecordRespVO> pages = examRecordService.selectToMarkPageList(vo);

        return new ResultAPI<>(ResponseConstant.SUCCESS,"",pages);
    }

    /**
     * 得到待批改的试卷
     * @param id
     * @return
     */
    @GetMapping("selectRecord/{id}")
    public ResultAPI<ExamViewVO> selectRecord(@PathVariable Integer id){

        ExamViewVO viewVO = examRecordService.selectRecordById(id);

        return new ResultAPI<>(ResponseConstant.SUCCESS,"",viewVO);
    }

    @PostMapping("correct")
    public ResultAPI<String> correct(@RequestBody ExamViewVO vo){
        examRecordService.correct(vo);
        return new ResultAPI<>(ResponseConstant.SUCCESS,"批改完成","");
    }

}
