package com.xyh.controller.teacher;

import com.xyh.base.ResultAPI;
import com.xyh.service.SubjectService;
import com.xyh.vo.response.other.KeyValue;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@PreAuthorize("hasAnyRole('TEACHER')")
@RequestMapping("teacher/subject")
@RestController("teacherSubjectController")
public class SubjectController {

    private SubjectService subjectService;

    @Autowired
    public SubjectController(SubjectService subjectService) {
        this.subjectService = subjectService;
    }

    @GetMapping("getSubjectName/{subjectName}")
    public ResultAPI<List<KeyValue>> getSubjectList(@PathVariable String subjectName){
        List<KeyValue> list = subjectService.getSubjectName(subjectName);
        return new ResultAPI<>(200,"查询成功",list);
    }

}
