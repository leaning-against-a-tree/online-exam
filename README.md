# 在线考试系统

#### 一、项目介绍
一个基于SpringBoot、SpringSecurity、Mybatis-Plus、Redis、Docker、RabbitMQ、Vue等技术栈实现的在线考试系统，实现了管理员进行系统管理，教师进行题目和试卷的创建、发布、编辑以及批阅试卷、学生进行题库练习、考试、错题集等主要功能的实现。


#### 二、项目演示
![输入图片说明](https://foruda.gitee.com/images/1694268706602388750/66e0eb3e_10805371.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1694268723226937779/863c6757_10805371.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1694268740273638775/402be13b_10805371.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1694268763689873401/1db72846_10805371.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1694268784203498583/b7c2c092_10805371.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1694268794472377708/328a297f_10805371.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1694268810355849045/1d6c306b_10805371.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1694268822652684129/c0a7e49c_10805371.png "屏幕截图")
#### 三、环境搭建
- 后端环境搭建
    1. SpringBoot2.6.7
    2. MySQL5.X/MySQL8.x
    3. Redis6.2.4
    4. RabbitMQ3.9.11
    5. CentOS7.X

#### 四、 特别说明

1.  master分支MySQL版本为5.X、modify分支MySQL版本为8.X
2.  modify分支添加了knife4j可以自动生成API文档，文档地址为http://{ip}:{port}/doc.html
3.  本仓库开源代码为后端代码，前端代码和SQL脚本地址为：https://gitee.com/leaning-against-a-tree/online-exam.git 前端代码后续有时间会进行修改

