
const path = require('path')

const { defineConfig } = require('@vue/cli-service');

const name = "在线教育学生端";

const port = 9526;

function resolve(dir){
  return  path.join(__dirname,dir);
}

module.exports = defineConfig({
  publicPath:'/',
  outputDir:'dist',
  assetsDir:'static',
  transpileDependencies: true,
  lintOnSave: false,
  //跨域代理
  devServer: {
    port:port,
    host:'localhost',
    proxy:{
      '/api':{
        target : 'http://localhost:8080',
        pathRewrite:{'^/api':''}
      }
    }
  },

  configureWebpack:{
    name:name,
    resolve: {
      alias: {
        '@':resolve('src')
      }
    }
  }
})
