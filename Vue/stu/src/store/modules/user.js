import { setToken,getToken } from "@/utils/auth";


const getDefaultState = ()=>{
    return {
        token:getToken(),
        username:'',
        imageUrl:''
    }
}

const state = getDefaultState();

const actions = {
    setToken(miniStore,val){
        miniStore.commit('SET_TOKEN',val)
    },

    setAvatar(miniStore,val){
        miniStore.commit('SET_IMAGEURL',val);
    }
}

const mutations = {
    SET_TOKEN(state,token){
        state.token = token;
        setToken(token);
    },
    // 这边设置头像
    SET_IMAGEURL(state,imageUrl){
        state.imageUrl = imageUrl;
    }
}

export default{
    namespaced: true,
    state,
    mutations,
    actions
}