// messageCount的共享数据
const state = {
    messageCount:0
}

const actions = {
    setCount(miniStore,val){
        console.log('传过来的值',val);
        miniStore.commit('setCount',val)
    },
    read(miniStore,val){
        miniStore.commit('read',val);
    }
}

const mutations = {
    read(state,val){
        state.messageCount -= val ;
        console.log(state.messageCount,'state.messageCount');
    },
    setCount(state,val){
        state.messageCount = val;
    }
}

export default{
    namespaced: true,
    state,
    mutations,
    actions
}