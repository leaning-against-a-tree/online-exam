const getters ={
    imageUrl: state => state.user.imageUrl,
    token: state => state.user.token
}
export default getters