import Vuex from 'vuex'
import Vue from 'vue';
Vue.use(Vuex)
// 导入消息的store
import message from "./modules/message";
import user from './modules/user';
import getters from './getter';

const store = new Vuex.Store({
    modules:{
        message,
        user
    },
    getters
})
export default store