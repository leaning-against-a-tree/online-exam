import Vue from 'vue'
import Router from 'vue-router'
import Layout from '@/components/layout/index'


// 使用路由
Vue.use(Router)

const routes = [
    {
        path:'/login/index',
        name:'Login',
        component: ()=>import('@/views/login/index'),    
        meta:{title:'用户登录',isAuth:false}
    },
    {
        path:'/register',
        name:'Register',
        component: ()=>import('@/views/register/index'),
        meta:{title:'用户注册',isAuth:false}, 
    },
    {
        path:'/login/retriveAccount',
        name:'RetrieveAccount',
        component:()=>import('@/views/login/retriveAccount'),
        meta:{title:'找回密码',isAuth:false}
    },
    {
        path:'/',
        component:Layout,
        redirect: '/index',
        children:[
            {
                path:'index',
                name:'DashBoard',
                component:()=>import('@/views/dashboard/index'),
                meta:{title:'首页',isAuth:true}
            }
        ]
    },
    {
        path:'/exam',
        component:Layout,
        children:[
            {
                path:'index',
                name:'Exam',
                component: ()=>import('@/views/exam/index'),
                meta:{title:'考试中心',isAuth:true}
            }
        ]
    },
    {
        path:'/exam/examPaper',
        name:'ExamPaper',
        component:()=>import('@/views/exam/examPaper'),
        meta:{title:'正在考试',isAuth:true}
    },
    {
        path:'/exam/examView',
        name:'ExamView',
        component:()=>import('@/views/exam/examView'),
        meta:{title:'查看考试',isAuth:true}
    },
    {
        path:'/records',
        component:Layout,
        children:[
            {
                path:'index',
                name:'Records',
                component: ()=>import('@/views/records/index'),
                meta:{title:'考试记录',isAuth:true}
            }
        ]
    },
    {
        path:'/question',
        component:Layout,
        children:[
            {
                path:'index',
                name:'Question',
                component: ()=>import('@/views/question/index'),
                meta:{title:'题库练习',isAuth:true}
            }
        ]
    },
    {
        path:'/question/practice',
        name:'Practice',
        component:()=>import('@/views/question/practice'),
        meta:{title:'题目训练',isAuth:true}
    },
    {
        path:'/wrongBook',
        component:Layout,
        children:[
            {
                path:'index',
                name:'WrongBook',
                component: ()=>import('@/views/wrongBook/index'),
                meta:{title:'错题本',isAuth:true}
            },
            {
                path:'read',
                name:'WrongRead',
                component: ()=>import('@/views/wrongBook/read'),
                meta:{title:'错题详情页',isAuth:true}
            },
        ]
    },
    {
        path:'/profile',
        component:Layout,
        children:[
            {
                path:'index',
                name:'Profile',
                component: ()=>import('@/views/profile/index'),
                meta:{title:'个人中心',isAuth:true}
            }
        ]
    },
    {
        path:'/message',
        component:Layout,
        children:[
            {
                path:'index',
                name:'Message',
                component: ()=>import('@/views/message/index'),
                meta:{title:'消息中心',isAuth:true}
            }
        ]
    }
]

const router = new Router({
    mode:'hash',
    routes
})
export default router;
