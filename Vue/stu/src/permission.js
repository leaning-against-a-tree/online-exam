import router from "./router";

import {getToken} from '@/utils/auth'
import { Message } from "element-ui";

/**
 * 需要获取到token
 */
router.beforeEach(async(to,from,next)=>{
    const hasToken = getToken();
    document.title = to.meta.title;
    if(!to.meta.isAuth){
        next();
    }else{
        if(hasToken){
            next();
        }else{
            Message.error("请重新登录")
            next(`/login/index?redirect=${new Date().getTime()}`)
        }
    }
})
