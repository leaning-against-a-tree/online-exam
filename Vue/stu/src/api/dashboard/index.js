// 首页的api
import request from '@/utils/request'

export function recentExam(data) {
    return request({
        url:'/api/student/dashboard/index',
        method:'get'
    })
}

export function chartData(data) {
    return request({
        url:'/api/student/dashboard/chart',
        method:'get'
    })
}