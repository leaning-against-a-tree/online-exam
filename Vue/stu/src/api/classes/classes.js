import request from '@/utils/request'

export function classesList(data){
    return request({
        url:`/api/student/classes/list/${data}`,
        method:'post'
    })
}