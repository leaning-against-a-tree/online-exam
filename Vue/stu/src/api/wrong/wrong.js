// 对于错题本的api

import request from '@/utils/request'

export function pageList(data){
    return request({
        url:'/api/student/wrong/list',
        method:'post',
        data
    })
}

export function wrongItems(data){
    return request({
        url:'/api/student/wrong/wrongList',
        method:'post',
        data
    })
}

export function removeWrong(data){
    return request({
        url:`/api/student/wrong/remove/${data}`,
        method:'get'
    })
}