// 对试卷中心api
import request from '@/utils/request'

export function getStuExamList(data) {
    return request({
        headers: { 'Content-Type': 'application/json', 'request-ajax': true },
        url:'/api/student/exam/list',
        method:'post',
        data
    })
}

export function getQuestionList(data){
    return request({
        url:`/api/student/exam/question/list/${data}`,
        method:'get'
    })
}

export function correctExam(data){
    return request({
        url:'/api/student/exam/correct',
        method:'post',
        data
    })
}