// 对消息的处理的api
import request from '@/utils/request'

export function getUnreadCount(data){
    return request({
        url:'/api/student/message/getUnread',
        method:'get'
    })
}

// 得到学生的信息列表
export function getStuMsgList(data){
    return request({
        headers: { 'Content-Type': 'application/json', 'request-ajax': true },
        url:'/api/student/message/list',
        method:'post',
        data
    })
}

// 阅读信息
export function readMessage(data){
    return request({
        url:`/api/student/message/read/${data}`,
        method:'get'
    })
}