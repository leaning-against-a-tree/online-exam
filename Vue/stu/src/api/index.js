// 统一暴露api的接口

import * as user from './user/user'
import * as dashboard from './dashboard/index'
import * as exam from './exam/exam'
import * as message from './message/message'
import * as practice from './practice/practice'
import * as records from './records/records'
import * as wrong from './wrong/wrong'
import * as classes from './classes/classes'

export default({
    user,
    dashboard,
    exam,
    message,
    practice,
    records,
    wrong,
    classes
})