// 对练习操作的api

import request from '@/utils/request'

export function getPracticeList(data){
    return request({
        url:'/api/student/practice/list',
        method:'post',
        data
    })
}

export function getAnswerCard(data){
    return request({
        url:`/api/student/practice/answerCard/${data}`,
        method:'get'
    })
}


export function getQuestion(data){
    return request({
        url:`/api/student/practice/question`,
        method:'post',
        data
    })
}

export function correctQuestion(data){
    return request({
        url:'/api/student/practice/correct',
        method:'post',
        data
    })
}