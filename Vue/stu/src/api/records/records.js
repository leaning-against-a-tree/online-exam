// 对考试记录的操作
import request from '@/utils/request'

export function pageList(data){
    return request({
        url:'/api/student/record/list',
        method:'post',
        data
    })
}

export function examViewList(data){
    return request({
        url:`/api/student/record/select/${data}`,
        method:'get'
    })
}