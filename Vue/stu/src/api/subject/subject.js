// 对科目操作的api
import request from '@/utils/request'

export function getSubjects(data){
    return request({
        url:'/api/student/subject/list',
        method:'get'
    })
}