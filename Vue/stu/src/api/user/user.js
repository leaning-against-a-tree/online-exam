import request from '@/utils/request'

export function login(data) {
    return request({
        // 问题一：模板并没有加api
        url:'/api/user/login',
        method:'post',
        data
    })
}

export function logout(data){
    return request({
        headers: { 'Content-Type': 'application/json', 'request-ajax': true },
        url:`api/user/logout/${data}`,
        method:'post'
    })
}

export function getInfo(data){
    return request({
        url:'/api/user/info?token='+data,
        method:'get'
    })
}

export function getCurrent(data){
    return request({
        url:'/api/common/user/current',
        method:'get'
    })
}

export function updateSelf(data){
    return request({
        url:'/api/common/user/updateSelf',
        method:'post',
        data
    })
}

export function updatePass(data){
    return request({
        url:'/api/common/user/updatePass',
        method:'post',
        data
    })
}

export function validAccount(data){
    return request({
        url:`/api/user/validAccount/${data}`,
        method:'get'
    })
}

export function register(data){
    return request({
        url:'/api/user/register',
        method:'post',
        data
    })
}

export function getValidCode(data){
    return request({
        url:`/api/user/sendCode/${data}`,
        method:'get'
    })
}

export function resetPass(data){
    return request({
        url:'/api/user/resetPass',
        method:'post',
        data
    })
}