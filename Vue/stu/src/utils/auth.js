// 对于token的设置与关闭
import Cookies from "js-cookie";

const TokenKey = 'student_xyh';
// 得到token
export function getToken(){
    return Cookies.get(TokenKey);
}

export function setToken(token){
    return Cookies.set(TokenKey,token)
}

export function removeToken(){
    return Cookies.remove(TokenKey);
}