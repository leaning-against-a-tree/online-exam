import Vue from 'vue'
import App from './App.vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import * as echarts from 'echarts'
import router from './router'
// 引入富文本
import tinymce from 'tinymce'
import VueTinymce from '@packy-tang/vue-tinymce'


// 进行路由守卫
import '@/permission'

import store from './store'



Vue.use(ElementUI);
Vue.prototype.$echarts = echarts;

// 富文本
Vue.prototype.$tinymce = tinymce
Vue.use(VueTinymce)


Vue.config.productionTip = false



new Vue({
  render: h => h(App),
  store,
  router
}).$mount('#app')
