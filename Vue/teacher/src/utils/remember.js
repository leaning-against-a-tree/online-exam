
const nameKey = "cookie-teacher-username";

const otherKey = 'cookie-teacher-other';

export function setNameKey(val){
    sessionStorage.setItem(nameKey,val)
}

export function setOtherKey(val){
    sessionStorage.setItem(otherKey,val);
}

export function getName(){
    return sessionStorage.getItem(nameKey);
}
export function getOther(){
    return sessionStorage.getItem(otherKey);
}

export function removeOtherKey(val){
    sessionStorage.removeItem(otherKey);
}