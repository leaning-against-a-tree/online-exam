/**
 * Created by PanJiaChen on 16/11/18.
 */

/**
 * @param {string} path
 * @returns {Boolean}
 */
export function isExternal(path) {
  return /^(https?:|mailto:|tel:)/.test(path)
}

/**
 * @param {string} str
 * @returns {Boolean}
 */
export function validUsername(str) {
  // const valid_map = ['admin', 'editor']
  return str.length > 0;
  //return valid_map.indexOf(str.trim()) >= 0
}
// 进行题目创建的时候不允许科目为空
export function validSubjectName(str){
  return str.length > 0;
}
