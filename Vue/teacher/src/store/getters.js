const getters = {
  sidebar: state => state.app.sidebar,
  device: state => state.app.device,
  token: state => state.user.token,
  imagePath: state => state.user.imagePath,
  name: state => state.user.username
}
export default getters
