// 初始化state
const state = {
    user:{
        sexEnum: [{key: 0 ,value:'男'},{key: 1, value:'女'}],
        statusEnum: [{key: 0 , value:'启用'},{key: 1 ,value:'禁用'}],
        roleEnum: [{key: 0 , value:'管理员'},{key: 1 ,value:'教师'},{key:2,value:'学生'}]
    }
}

// getters
const getters = {
    enumFormat: (state) => (arrary, key) => {
        console.log('@arrary',arrary);
        console.log('@key',key);
        return format(arrary, key)
        }
    }
const actions = {}
const mutations = {}

const format = function (array, key) {
    for (let item of array) {
        if (item.key === key) {
            return item.value
            }
        }
        return null
    }
    export default {
        namespaced: true,
        state,
        getters,
        actions,
        mutations
    }