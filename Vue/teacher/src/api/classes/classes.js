import request from '@/utils/request'

export function classesList(data){
    return request({
        url:`/student/classes/list/${data}`,
        method:'post'
    })
}

export function pageList(data){
    return request({
        headers: { 'Content-Type': 'application/json', 'request-ajax': true },
        url:'/teacher/classes/list',
        method:'post',
        data
    })
}

export function selectById(data){
    return request({
        url:`/teacher/classes/select/${data}`,
        method:'get'
    })
}

export function edit(data){
    return request({
        headers: { 'Content-Type': 'application/json', 'request-ajax': true },
        url:'/teacher/classes/edit',
        method:'post',
        data
    })
}

export function delBatch(data){
    return request({
        headers: { 'Content-Type': 'application/json', 'request-ajax': true },
        url:'/teacher/classes/dels',
        method:'post',
        data
    })
}