import request from '@/utils/request'

export const  getSubjectName = (data) => request({
    url:`/teacher/subject/getSubjectName/${data}`,
    method:'get'
})