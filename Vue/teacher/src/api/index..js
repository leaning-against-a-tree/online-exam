// 将模块中的接口方法统一暴露
import * as subject from './subject/subject'
import * as dashboard from './dashboard/dashboard'
import * as log from './log/log'
import * as profile from './profile/profile'
import * as question from './question/question'
import * as exam from './exam/exam'
import * as record from './exam/record'
import * as classes from './classes/classes'



export default{
    subject,
    dashboard,
    log,
    profile,
    question,
    exam,
    classes,
    record
}