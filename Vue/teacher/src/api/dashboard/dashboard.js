// 针对于首页所发送的请求

import request from '@/utils/request'

// 主页
export const dashboard = (data)=> request({
    url: 'teacher/dashboard/index',
    method:'get'
})