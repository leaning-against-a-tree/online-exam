import request from '@/utils/request'

export const currentUser = (data) => request({
    url: '/common/user/current',
    method : 'get',
})

export const updateSelf =(data) =>request({
    headers: { 'Content-Type': 'application/json', 'request-ajax': true },
    url: '/common/user/updateSelf',
    method : 'post',
    data
})

export const updatePass =(data) =>request({
    headers: { 'Content-Type': 'application/json', 'request-ajax': true },
    url: '/common/user/updatePass',
    method : 'post',
    data
})