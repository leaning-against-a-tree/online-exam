import request from '@/utils/request'

export const pagesList = (data)=> request({
    headers: { 'Content-Type': 'application/json', 'request-ajax': true },
    url: '/teacher/record/list',
    method : 'post',
    data
})


export const selectById = (data)=> request({
    url: `/teacher/record/selectRecord/${data}`,
    method : 'get',
})

export const correctExam = (data)=> request({
    headers: { 'Content-Type': 'application/json', 'request-ajax': true },
    url: '/teacher/record/correct',
    method : 'post',
    data
})