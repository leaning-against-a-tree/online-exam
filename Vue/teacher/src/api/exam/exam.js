import request from '@/utils/request'

export const pagesList = (data)=> request({
    headers: { 'Content-Type': 'application/json', 'request-ajax': true },
    url: '/teacher/exam/page/list',
    method : 'post',
    data
})

export const selectById = (data) =>request({
    url:`/teacher/exam/getExamById/${data}`,
    method : 'get'
})

export const edit = (data) => request({
    headers: { 'Content-Type': 'application/json', 'request-ajax': true },
    url: '/teacher/exam/edit',
    method : 'post',
    data
})

export const delById = (data) =>request({
    url:`/teacher/exam/delById/${data}`,
    method : 'get'
})

export const autoGroup = (data) => request({
    url:'/teacher/exam/autoGroup',
    method:'post',
    data
})

export function scoreList(data){
    return request({
        url:'/teacher/exam/scoreList',
        method:'post',
        data
    })
}

export const scoreDetail = (data) =>request({
    url:`/teacher/exam/analysis/${data.examId}/${data.classesId}`,
    method : 'get'
})