import request from '@/utils/request'

export const pagesList = (data)=> request({
    headers: { 'Content-Type': 'application/json', 'request-ajax': true },
    url: '/teacher/question/page/list',
    method : 'post',
    data
})

export const delByIds = (data) =>request({
    headers: { 'Content-Type': 'application/json', 'request-ajax': true },
    url: '/teacher/question/delByIds',
    method : 'post',
    data
})

export const previewQuestion = (data) => request({
    url: `teacher/question/preview/${data}`,
    method : 'get',
})

export const getQuestionById = (data) => request({
    url: `teacher/question/getQuestionById/${data}`,
    method : 'get',
})

export const editOrAdd = (data) => request({
    headers: { 'Content-Type': 'application/json', 'request-ajax': true },
    url: '/teacher/question/edit',
    method : 'post',
    data
})

export const addPagesList = (data)=> request({
    headers: { 'Content-Type': 'application/json', 'request-ajax': true },
    url: '/teacher/question/add/page/list',
    method : 'post',
    data
})