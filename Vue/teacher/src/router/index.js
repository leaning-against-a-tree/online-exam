import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      name: 'Dashboard',
      component: () => import('@/views/dashboard/index'),
      meta: { title: '首页', icon: 'dashboard' }
    }]
  },
  {
    path:'/classes',
    name:'ClassesIndex',
    component:Layout,
    meta:{title:'班级管理',icon:'el-icon-s-grid'},
    alwaysShow:true,
    children:[
      {

        path:'index',
        name:'ClassesIndex',
        component:()=>import('@/views/classess/index'),
        meta:{title:'班级列表',icon:'el-icon-menu'}
      }
    ]
  },

  {
    path: '/question',
    component: Layout,
    name: 'Question',
    meta: {title : '题库管理' , icon : 'el-icon-collection'},
    children: [
      {
        path: 'index',
        name: 'QuestionList',
        component: () => import('@/views/question/index'),
        meta: {title: '题目列表' ,icon: 'el-icon-tickets'}
      },
      {
        path:'singleQuestion',
        name:'SingleQuestion',
        component: ()=> import('@/views/question/singleQuestion'),
        meta:{title: '单选题创编',icon:'el-icon-edit-outline'}
      },
      {
        path:'mutilQuestion',
        name:'MutilQuestion',
        component: ()=> import('@/views/question/mutilQuestion'),
        meta:{title: '多选题创编',icon:'el-icon-edit-outline'}
      },
      {
        path:'judgeQuestion',
        name:'JudgeQuestion',
        component: ()=> import('@/views/question/judgeQuestion'),
        meta:{title: '判断题创编',icon:'el-icon-edit-outline'}
      },
      {
        path:'writtenQuestion',
        name:'WrittenQuestion',
        component: ()=> import('@/views/question/writtenQuestion'),
        meta:{title: '简答题创编',icon:'el-icon-edit-outline'}
      }
    ]
  },
  {
    path:'/exam',
    name:'Exam',
    component:Layout,
    meta:{title:'试卷管理',icon:'el-icon-s-management'},
    alwaysShow:true,
    children:[
      {
        path:'index',
        name:'ExamList',
        component:()=> import('@/views/exam/index'),
        meta:{title:'试卷列表',icon:'el-icon-document-copy'}
      },
      {
        path:'edit',
        name:'ExamEdit',
        component:()=> import('@/views/exam/edit'),
        meta:{title:'试卷创编'},
        hidden:true,
      },
      {
        path:'autoGroup',
        name:'AutoGroup',
        component:()=> import('@/views/exam/autoGroup'),
        meta:{title:'自动组卷'},
        hidden:true,
      }
    ]
  },
  {
    path:'/score',
    name:'Score',
    component: Layout,
    meta:{title:'成绩管理',icon:'el-icon-menu'},
    alwaysShow:true,
    children:[
      {
        path:'scoreList',
        name:'ScoreList',
        component: ()=>import('@/views/score/scoreList'),
        meta:{title:'成绩列表',icon:'el-icon-s-order'}
      },
      {
        path:'scoreAnalysis',
        name:'ScoreAnalysis',
        component: ()=>import('@/views/score/scoreAnalysis'),
        hidden:true,
        meta:{title:'成绩分析'}
      },
      {
        path:'toBeMarkList',
        name:'ToBeMarkList',
        component:()=>import('@/views/score/toBeMarkList'),
        meta:{title:'待批改列表',icon:'el-icon-notebook-2'}
      },
      {
        path:'markExam',
        name:'MarkExam',
        component: ()=>import('@/views/score/markExam'),
        meta:{title:'试卷批改'},
        hidden:true
      }
    ]
  },
  // 个人中心搭建
  {
    path:'/profile',
    component: Layout,
    hidden: true,
    children:[
      {
        path: 'index',
        name: 'Profile',
        component: ()=> import('@/views/profile/index'),
        meta: {title: '个人中心',noCache: true}
      }
    ]
  },
  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
