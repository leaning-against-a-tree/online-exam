import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      name: 'Dashboard',
      component: () => import('@/views/dashboard/index'),
      meta: { title: '首页', icon: 'dashboard' }
    }]
  },

  {
    path: '/user',
    component: Layout,
    name: 'User',
    meta: {title : '信息管理' , icon : 'el-icon-user'},
    children: [
      {
        path: 'teacher',
        name: 'Teacher',
        component: () => import('@/views/user/teacher'),
        meta: {title: '教师管理' ,icon: 'el-icon-s-custom'}
      },
      {
        path: 'teacher/edit',
        hidden: true,
        name: 'editTeacher',
        component: () => import('@/views/user/teacher/edit'),
        meta: {title: '教师新增/编辑'}
      },
      {
        path: 'student',
        name: 'Student',
        component: () => import('@/views/user/student'),
        meta: {title: '学生管理' ,icon: 'el-icon-user-solid'}
      },
      {
        path: 'student/edit',
        hidden: true,
        name: 'editStudent',
        component: () => import('@/views/user/student/edit'),
        meta: {title: '学生查看/编辑'}
      }
    ]
  },
  {
    path: '/subject',
    name: 'Subject',
    component: Layout,
    meta: {title: '科目管理' , icon: 'education'},
    alwaysShow: true,
    children:[
      {
        path: 'index',
        name: 'SubjectList',
        component: ()=> import('@/views/subject/index'),
        meta: {title: '科目列表',icon:'el-icon-tickets'}
      },
      {
        path: 'edit',
        name: 'SubjectEdit',
        component: ()=> import('@/views/subject/edit'),
        meta:{title:'学科创编'},
        hidden: true
      }
    ]
  },
  {
    path: '/message',
    component: Layout,
    name: 'Message',
    meta: {title: '公告管理',icon:'el-icon-message'},
    children: [
      {
        path: 'index',
        name: 'MessageList',
        component: ()=> import('@/views/message/index'),
        meta: {title: '公告列表',icon: 'el-icon-chat-line-square'}
      },
      {
        path: 'send',
        name: 'MessageSend',
        component: ()=> import('@/views/message/send'),
        meta: {title: '公告创建',icon: 'el-icon-position'}
      },
      {
        path: 'detail',
        name: 'Detail',
        component: ()=> import('@/views/message/detail'),
        meta: {title: '详情'},
        hidden: true
      }
    ]
  },
  {
    path: '/log',
    component: Layout,
    name: 'Log',
    meta: {
      title: '日志管理',
      icon: 'el-icon-s-order',
    },
    alwaysShow: true,
    children:[
      {
        path: 'index',
        name: 'UserLog',
        component: ()=> import('@/views/log/index'),
        meta:{
          title:'用户日志',
          icon: 'el-icon-notebook-1',
          noCache: true
        }
      }
    ]
  },
  {
    path: '/census',
    component: Layout,
    name: 'Census',
    meta: {
      title: '统计管理',
      icon: 'census',
    },
    alwaysShow: true,
    children:[
      {
        path: 'index',
        name: 'UserLog',
        component: ()=> import('@/views/census/index'),
        meta:{
          title:'访问量统计',
          icon: 'person-active',
          noCache: true
        }
      }
    ]
  },
  // 个人中心搭建
  {
    path:'/profile',
    component: Layout,
    hidden: true,
    children:[
      {
        path: 'index',
        name: 'Profile',
        component: ()=> import('@/views/profile/index'),
        meta: {title: '个人中心',noCache: true}
      }
    ]
  },
  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
