// 针对于首页所发送的请求

import request from '@/utils/request'

// 管理员的主页
export const adminDashboard = (data)=> request({
    url: 'admin/dashboard/index',
    method:'get'
})