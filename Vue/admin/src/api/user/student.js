// 用户管理的API

import request from '@/utils/request';

// 获取学生列表
export const pageList =  (data) => request({
        headers: { 'Content-Type': 'application/json', 'request-ajax': true },
        url: '/admin/user/page/list',
        method : 'post',
        data
    })
// 获取单个学生的信息
export const getUserById = (data) =>request({
    url: `/admin/user/select/${data}`,
    method: 'get'
})

// 修改/添加学生信息
export const updateUser = (data) => request({
    headers: { 'Content-Type': 'application/json', 'request-ajax': true },
    url:'admin/user/edit',
    method: 'post',
    data
})

// 删除学生信息
export const deleteUser = (data) =>request({
    url: `/admin/user/delete/${data}`,
    method: 'get'
})

// 修改状态
export const updateStatus = (data) =>request({
    headers: { 'Content-Type': 'application/json', 'request-ajax': true },
    url:'/admin/user/updateStatus',
    method:'post',
    data
})

// 查询用户信息
export const getUserByUserName = (data) =>request({
    url: `/admin/user/selectUser/${data}`,
    method: 'get'
})

