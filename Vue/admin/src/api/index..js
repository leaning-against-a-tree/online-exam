// 将模块中的接口方法统一暴露
import * as student from './user/student'
import * as subject from './subject/subject'
import * as message from './message/message'
import * as dashboard from './dashboard/dashboard'
import * as log from './log/log'
import * as profile from './profile/profile'

import * as census from './census/census'

export default{
    student,
    subject,
    message,
    dashboard,
    profile,
    log,
    census
}