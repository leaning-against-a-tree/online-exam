import request from '@/utils/request'

// 得到公告分页
export const pageList =(data) =>request({
    headers: { 'Content-Type': 'application/json', 'request-ajax': true },
    url: '/log/page/list',
    method : 'post',
    data
})

export const userLog =(data) =>request({
    url: '/common/user/log',
    method : 'get',
})