// 消息相关的处理
import request from '@/utils/request'

// 得到公告分页
export const pageList =(data) =>request({
    headers: { 'Content-Type': 'application/json', 'request-ajax': true },
    url: '/admin/message/page/list',
    method : 'post',
    data
})

// 发送消息
export const sendMsg = (data) => request({
    headers: { 'Content-Type': 'application/json', 'request-ajax': true },
    url: '/admin/message/send',
    method : 'post',
    data
})
