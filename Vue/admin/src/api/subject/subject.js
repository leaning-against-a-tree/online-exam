import request from '@/utils/request'

//  带条件分页查询
export const pageList =  (data) => request({
    headers: { 'Content-Type': 'application/json', 'request-ajax': true },
    url: '/admin/subject/page/list',
    method : 'post',
    data
})

// 删除/可批量
export const delByIds = (data)=>request({
    headers: { 'Content-Type': 'application/json', 'request-ajax': true },
    url: '/admin/subject/delByIds',
    method : 'post',
    data
})


// id查询
export const getSubjectById = (data) => request({
    url:`/admin/subject/select/${data}`,
    method:'get'
})

// 编辑/添加
export const editSubject = (data) => request({
    headers: { 'Content-Type': 'application/json', 'request-ajax': true },
    url: '/admin/subject/edit',
    method : 'post',
    data
})