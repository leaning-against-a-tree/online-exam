import request from '@/utils/request'

export const getVisitCount = (data) => request({
    url:`admin/census/visit`,
    method:'get'
})


export const activeData =(data) =>request({
    headers: { 'Content-Type': 'application/json', 'request-ajax': true },
    url: 'admin/census/activeData',
    method : 'post',
    data
})